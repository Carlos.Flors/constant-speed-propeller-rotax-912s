
#include <p18F4520.h>	// Libreria de declaración de registros especiales
#include <math.h>


float RPM;
float RPM_goal;
float angle;
float PI;
float radian_conversion;
float throttle;
float speed;
float idle_rpm;
float sine;
float cosine;
float timerzero;
float error;

unsigned int pitch_display;
unsigned int RPM_display;
unsigned int speed_display;
unsigned int timerzero_int;
unsigned long RPM_binary;
unsigned long pitch_binary;
unsigned long speed_binary;
unsigned int conversion_throttle;
unsigned int conversion_speed;
unsigned int uni;
unsigned int dec;
unsigned int cen;
unsigned int mil;
unsigned int n;
unsigned int p_uni;
unsigned int p_dec;
unsigned int p_cen;
unsigned int p_n;
unsigned int s_uni;
unsigned int s_dec;
unsigned int s_cen;
unsigned int s_n;

void R_Int_Alta(void); // High priority interrupt routine declaration




#pragma code Vector_Int_Alta=0x08 // High priority interrupt vector
void Int_Alta (void)
{ _asm GOTO R_Int_Alta _endasm }
#pragma code




#pragma interrupt R_Int_Alta // High priority interrupt routine
void R_Int_Alta (void)
{ 
    if (INTCONbits.TMR0IF==1) // Check if interrupt is caused by timer 0 overflow
    {
        INTCONbits.TMR0IF=0; // Interrupt flag is reset
        if (PORTDbits.RD0)
        {
            PORTDbits.RD0=0; // if RD0 is 1 then it is reset to 0
            TMR0H=45536/256; // TMR0H and TMR0L are reloaded for 20ms
            TMR0L=45536%256; 
        }
        else
        {
            PORTDbits.RD0=1; // if RD0 is 0 it is set to 1
            TMR0H=timerzero_int/256; // TMR0H and TMR0L are reloaded for the next run
            TMR0L=timerzero_int%256; // 
        }
    }
}



void main (void)   
{
    TRISB=0x00;        // Port B set as output
    TRISD=0x00;        // Port D set as output
    TRISC=0x00;        // Port C set as output.
    
    ADCON0=0x01;    // A/D converter is activated and channel AN0 is selected
    ADCON1=0b00001101;   // AN0 is established as A/D channel 
    ADCON2=0b10111100;  // A/D conversion result is right justified. Acquisition time=20*TADC
                         // A/D converter clock signal is configured to FOSC/4 (TADC=4/(4*10^6)=1us)
    

    INTCONbits.GIE=1; // Interrupts are enabled globally 
    INTCONbits.TMR0IE=1; // Timer 0 interrupt is enabled
    
    /*Defined starting variables*/
    PI=3.14159265; 
    radian_conversion = 0.01745329; // Pi / 180
    RPM = 1600;
    RPM_goal = 4201;
    speed = 0; 
    angle = 65; //starting angle, neutral.
    idle_rpm=1600;
    error=1.35; //error in timer0 in terms of induced pitch angle shift

    timerzero= 65536-(((500+((angle-error)*22.2222))*pow(10,-6)*4000000)/4);         
    timerzero_int= timerzero; 
    T0CON=0x88; // Timer 0 in 16-bit timer mode. Prescaler disabled. Timer ON
    
    TMR0H=timerzero_int/256; 
    TMR0L=timerzero_int%256; 
    
    
    while (1)				// Bucle sin fin
    {
        
        ADCON0=0b00000001;    // A/D converter is activated for channel AN0 (Throttle)
        ADCON0bits.GO=1;// Conversion is started
        while (ADCON0bits.GO==1); // End of conversion wait loop
        conversion_throttle = 256*ADRESH+ADRESL; // Right justified. Move two most significant bits of ADRESH 8 positions 
                                                  //and add the ADRESL 
        throttle = conversion_throttle; // int to float
        throttle = throttle*100/1023; //0-100% throttle

        
        ADCON0=0b00000101;    // A/D converter is activated for channel AN1 (Speed)
        ADCON0bits.GO=1;// Conversion is started
        while (ADCON0bits.GO==1); // End of conversion wait loop
        conversion_speed=ADRESL+ADRESH*256;  // Right justified. Move two most significant bits of ADRESH 8 positions 
                                             //and add the ADRESL 
        speed=conversion_speed; // int to float
        speed=(speed*400/1023); //Speed envelope 0 - 400 km/h
 
        
        //Mathematic blocks 
        
        sine = sin(angle*radian_conversion);
        cosine = cos(angle*radian_conversion);               
                
        //RPM calculation
        
        RPM =(sine + 0.0055 * throttle) * 27 * throttle + idle_rpm + (sine + 0.01) * pow(speed,1.5);
                

        //block that processes  RPM_display bits to the shift register
  
       /*
        example -> 4600 rpm -> must convert it first to binary.
         How??
        =0001 0001 1111 1000? ; 14 bit
        need to scan all these bits starting from the least significant
        so I have to send the last bit 16 times,
        after each sending, shift one bit to the right to send the next least significant bit.
        so first the output = rpm%2 -> last bit
        and then the output = rpm/2 -> shift bit
        and then repeat the instructions 15 more times.
        
        */ 
        RPM_display=RPM;  //from float to int, necessary for displays. It also removes any decimals
        uni=RPM_display%10;
        dec=(RPM_display%100)/10;
        cen=(RPM_display%1000)/100;
        mil=RPM_display/1000;
        
        RPM_binary= 4096*mil + 256*cen + 16*dec + uni; // binary 
        
        LATCbits.LATC2=0; // STB = 0, hide the mess
        for (n=0; n<16; n++) // 16bit
        {
            LATCbits.LATC0=RPM_binary%2; //number is assigned
            RPM_binary=RPM_binary/2; // Next bit
            LATCbits.LATC1=1; // Skip to Next number
            LATCbits.LATC1=0; // takes one machine code processing time to perform this action      
        }

        if (RPM>9999)  // prevent values over 9999
        {   
           LATDbits.LATD4=1; // LED overreving ON
           RPM_binary= 39321; // binary for 9999
            for (n=0; n<16; n++) // 16bit
            {
                LATCbits.LATC0=RPM_binary%2; //number is assigned
                RPM_binary=RPM_binary/2; // Next bit
                LATCbits.LATC1=1; // Skip to Next number
                LATCbits.LATC1=0; // takes one machine code processing time to perform this action      
            }
            LATCbits.LATC2=1; // STB = 1, display result
        }
        else if (RPM>7000)
        {   
            LATDbits.LATD4=1; // LED overreving ON
            LATCbits.LATC2=1; // STB = 1, display result      
        }
        else
        {
            LATDbits.LATD4=0; // LED overreving OFF
            LATCbits.LATC2=1; // STB = 1, display result     
        }

        
        //Code for the shift registor to display angle.
        pitch_display=angle*10; //from float to int, include one decimal

        p_uni=pitch_display%10;
        p_dec=(pitch_display%100)/10;
        p_cen=(pitch_display%1000)/100;
 
        pitch_binary= 256*p_cen + 16*p_dec + p_uni; // binary 
        
        LATCbits.LATC5=0; // STB = 0, hide the mess
        for (p_n=0; p_n<12; p_n++) // 12bit
        {
            LATCbits.LATC3=pitch_binary%2; //number is assigned
            pitch_binary=pitch_binary/2; // Next bit
            LATCbits.LATC4=1; // Skip to Next number
            LATCbits.LATC4=0; // takes one machine code processing time to perform this action      
        }
        LATCbits.LATC5=1; // STB = 1, display result     
        
        //Code for the shift registor to display airspeed.
        speed_display=speed; 

        s_uni=speed_display%10;
        s_dec=(speed_display%100)/10;
        s_cen=(speed_display%1000)/100;
 
        speed_binary= 256*s_cen + 16*s_dec + s_uni; // binary 
        
        LATDbits.LATD3=0; // STB = 0, hide the mess
        for (s_n=0; s_n<12; s_n++) // 12bit
        {
            LATDbits.LATD1=speed_binary%2; //number is assigned
            speed_binary=speed_binary/2; // Next bit
            LATDbits.LATD2=1; // Skip to Next number
            LATDbits.LATD2=0; // takes one machine code processing time to perform this action      
        }
        LATDbits.LATD3=1; // STB = 1, display result          
        
        
        if (PORTAbits.RA2==1)   //Check the feather switch
        {
        //code that forces motor to coarsen the pitch
        angle=angle-0.1;
        timerzero= 65536-(((500+((angle-error)*22.2222))*pow(10,-6)*4000000)/4); 
        timerzero_int= timerzero; 
        }
        else if (RPM<RPM_goal)
        {
        //code that forces motor to fine the pitch
        angle=angle+0.1;
        timerzero= 65536-(((500+((angle-error)*22.2222))*pow(10,-6)*4000000)/4); 
        timerzero_int= timerzero; 
        }
        else if (RPM>RPM_goal)
        {
        //code that forces motor to coarsen the pitch
        angle=angle-0.1;
        timerzero= 65536-(((500+((angle-error)*22.2222))*pow(10,-6)*4000000)/4); 
        timerzero_int= timerzero; 
        }
        else{}; // do nothing
        
        if (angle<0.1)
        {   
            angle=0.1;
        }
        else if (angle>89.9)
        {   
            angle=89.9;
        } 
        
        
    }
}
